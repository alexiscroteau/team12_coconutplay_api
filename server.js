require('dotenv').config();
import express from "express"
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './router.js'
import game from "./model/game"
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const app = express();
const PORT = 3000;
const JWT_SECRET = "supersecretphrasetocreatethetokensignature";
const mongoose = require('mongoose');
const DB_URL = 'mongodb://localhost/mydb';

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(express.json());

mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true,});
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connecté à la BD'));

const User = require('./model/userSchema');


//Seulement pour tester, jamais appelé sur le site
app.get('/users', async (req, res) =>{
  try{
    const usersdb = await User.find();
    res.json(usersdb);
  } catch(e){
    res.status(500).json({ message: err.message});
  }
});

app.post('/register', async (req, res) => {
  try{
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(req.body.password, salt)

    User.init().then(async function(){
      try{
        const user = new User({
          username: req.body.username, 
          password: hashedPassword,
          email: req.body.email,
          name: req.body.name,
          firstname: req.body.firstname,
          birthdate: req.body.birthdate,
          role: "Member"
        })
        
        const newUser = await user.save();
        res.status(201).json(newUser);
      } catch(e){
        res.status(409).send('User already exists');
      }
    });
  }
  catch (e){
    res.status(500).json({ message: e.message });
  }
})

app.post('/login', async (req,res) => {
  let qusername;
  let qpassword;
  await User.find({username: req.body.username}, (err,user) => {
    if (user == null){
      return res.status(400).send('User not found');
    }
    qusername = user[0].username;
    qpassword = user[0].password;
  });
  try{
    const user = { username: qusername, password: qpassword};
    if(await bcrypt.compare(req.body.password, user.password)) {
      const accessToken = jwt.sign(user, JWT_SECRET, { expiresIn: 300}, (err, accessToken) => {
        if(err) {
          res.status(500).send("Error");
        }
      
        res.send(accessToken);
      });
    }
    else{
      res.send('Wrong password');
    }
  }
  catch(e) {
    res.status(500).json({ message: e.message });
  }
})

app.get('/profile/me', authenticateToken, async (req, res) => {
    let secret;
  try{
    await User.find({username: req.user.username}, (err,user) => {
      secret = {
        username: user[0].username,
        email: user[0].email,
        name: user[0].name,
        firstname: user[0].firstname,
        birthdate: user[0].birthdate,
        role: user[0].role
      }
      res.json(secret);
    })
  } catch(e){
    res.status(500).send();
  }
})

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'] || req.headers['Authorization'];
  const authType = authHeader.split(' ')[0];
  const authToken = authHeader.split(' ')[1];

  if(authType != "Bearer"){
    return res.sendStatus(403);
  }

  if(authToken == null){
    return res.sendStatus(401);
  }

  jwt.verify(authToken, JWT_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  })
}

//Root route to test the API status
app.get('/', (req, res) => {
  res.send("Server is listening...");
});

//Inject the different routes availables
routes(app);

// If the route doesn't exist, send 404 error
app.use((req, res) => {
  res.status(404).send({url: `${req.originalUrl} not found`})
});

game.generate()
  .then(() => {
    //The server is now able to listen the request
    app.listen(PORT, () =>
      console.log(`Node server running on ${PORT}!`),
    );
  })
  .catch(() => {
    console.log("Error to generate default game test.")
  });
